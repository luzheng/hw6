const express = require('express');
const bodyParser = require('body-parser');
const cassandra = require('cassandra-driver');
const formidable = require('formidable');
const fs = require('fs');

const app = express();
const port = 3000;

const client = new cassandra.Client({contactPoints: ['127.0.0.1'], localDataCenter: "datacenter1", keyspace: 'hw6'});
client.connect((err, res) =>{
    console.log("Successfully connected to Cassandra");
});

app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

const insertStatement = "INSERT INTO hw6.imgs(filename, contents, type) VALUES (?, ?, ?)";
app.post('/deposit', function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, (err, fields, files) =>{
        if(err){
            console.log(err);
            res.status(500);
            res.end();
            return;
        }

        let imgBuffer = Buffer.from(fs.readFileSync(files.contents.path, 'base64'), 'base64');

        client.execute(insertStatement, [fields.filename, imgBuffer, files.contents.type], (err, file) =>{
            if(err){
                console.log(err);
                res.status(500);
                res.end();
            }else{
                res.json("File Uploaded");
            }
        });
    });
});

const getStatement = "SELECT contents, type FROM imgs WHERE filename = ?";
app.get('/retrieve', function(req, res) {
    let filename = req.body.filename || req.query.filename;
    client.execute(getStatement, [filename], (err, file) =>{
        if(err){ 
            console.log(err);
            res.status(404);
            res.end();
        }else{
            if(file.rowLength == 0){
                res.status(404);
                return;
            }

            res.writeHead(200, {'Content-Type': file.rows[0].type});
            res.write(file.rows[0].contents);
            res.end();
        }
    });
});

app.listen(port, () => console.log(`Server is listening on port ${port}`));